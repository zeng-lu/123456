document.getElementById('myButton').addEventListener('click', function() {
    // 获取按钮当前的背景颜色
    var currentColor = this.style.backgroundColor;
    
    // 判断当前颜色并切换到另一种颜色
    if (currentColor === 'lightblue') {
        this.style.backgroundColor = 'salmon';
    } else {
        this.style.backgroundColor = 'lightblue';
    }
});